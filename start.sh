if [[ -z "${FOURSCORE_AUTH_PATH}" ]]; then
  export FOURSCORE_AUTH_PATH=.
fi

cp ${FOURSCORE_AUTH_PATH}/target/FourScoreAPI-Auth-*.jar $FOURSCORE_AUTH_PATH

docker-compose -f docker-compose.yaml up