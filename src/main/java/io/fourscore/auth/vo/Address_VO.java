/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.vo;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Address_VO {
  private static Logger log = LoggerFactory.getLogger(Address_VO.class);
  
  @JsonProperty
  private String address1="";
  @JsonProperty
  private String address2="";
  @JsonProperty
  private String address3="";
  @JsonProperty
  private String address4="";
  @JsonProperty
  private String city="";
  @JsonProperty
  private String county="";
  @JsonProperty
  private String state="";
  @JsonProperty
  private String zip="";
  @JsonProperty
  private String school_district="";
  @JsonProperty
  private BigDecimal latitude;
  @JsonProperty
  private BigDecimal longitude;
  @JsonProperty
  private String place_id;
  
  public Address_VO() {}
  
  public Address_VO(String addr1, String addr2, String addr3, String addr4, 
      String city, String county, String state, String zip, String school_district, 
      BigDecimal latitude, BigDecimal longitude, String place_id){
    this.address1 = addr1;
    this.address2 = addr2;
    this.address3 = addr3;
    this.address4 = addr4;
    this.city = city;
    this.county = county;
    this.state = state;
    this.zip = zip;
    this.school_district = school_district;
    this.latitude = latitude;
    this.longitude = longitude;
    this.place_id = place_id;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getAddress3() {
    return address3;
  }

  public void setAddress3(String address3) {
    this.address3 = address3;
  }

  public String getAddress4() {
    return address4;
  }

  public void setAddress4(String address4) {
    this.address4 = address4;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getZip() {
    return zip;
  }

  public void setZip(String zip) {
    this.zip = zip;
  }

  public String getSchool_district() {
    return school_district;
  }

  public void setSchool_district(String school_district) {
    this.school_district = school_district;
  }

  public BigDecimal getLatitude() {
    return latitude;
  }

  public void setLatitude(BigDecimal latitude) {
    this.latitude = latitude;
  }

  public BigDecimal getLongitude() {
    return longitude;
  }

  public void setLongitude(BigDecimal longitude) {
    this.longitude = longitude;
  }

  public String getPlace_id() {
    return place_id;
  }

  public void setPlace_id(String place_id) {
    this.place_id = place_id;
  }
  
  public Address_VO sanitizeAddress(){
    boolean valid = true;
    Address_VO clean = new Address_VO();
    log.trace("Sanitizing Address");
    
    return clean;
  }
}
