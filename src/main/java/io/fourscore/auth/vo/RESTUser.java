/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.vo;

import java.security.Principal;
import java.util.List;

public class RESTUser implements Principal {
  private String userName;
  private List<String> role;
  
  public RESTUser(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  @Override
  public String getName() {
    return this.userName;
  }
  
  public List<String> getRole() {
    return this.role;
  }
  
  public void setRole(List<String> role) {
    this.role = role;
  }
}
