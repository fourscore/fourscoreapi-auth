/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Voter_VO {
  @JsonProperty
  private long id;
  @JsonProperty
  private String user_uuid;
  @JsonProperty
  private String user_name;
  @JsonProperty
  private String password;
  @JsonProperty
  private String new_password;
  @JsonProperty
  private String first_name;
  @JsonProperty
  private String middle_name;
  @JsonProperty
  private String last_name;
  @JsonProperty
  private String email;
  @JsonProperty
  private Address_VO address;

  public Voter_VO() {}
  
  public Voter_VO (long id, String user_uuid, String user_name, String password,
      String new_password, String first_name, String middle_name, 
      String last_name, String email, Address_VO address) {
    this.id = id;
    this.user_name = user_name;
    this.first_name = first_name;
    this.last_name = last_name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUser_uuid() {
    return user_uuid;
  }

  public void setUser_uuid(String user_uuid) {
    this.user_uuid = user_uuid;
  }

  public String getUser_name() {
    return user_name;
  }

  public void setUser_name(String user_name) {
    this.user_name = user_name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getNew_password() {
    return new_password;
  }

  public void setNew_password(String new_password) {
    this.new_password = new_password;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getMiddle_name() {
    return middle_name;
  }

  public void setMiddle_name(String middle_name) {
    this.middle_name = middle_name;
  }

  public String getLast_name() {
    return last_name;
  }

  public void setLast_name(String last_name) {
    this.last_name = last_name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Address_VO getAddress() {
    return address;
  }

  public void setAddress(Address_VO address) {
    this.address = address;
  }
}
