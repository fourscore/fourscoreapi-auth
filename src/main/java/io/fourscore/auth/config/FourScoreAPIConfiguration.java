/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.config;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FourScoreAPIConfiguration {
  @NotEmpty
  private String version;
  @NotEmpty
  private String server;
  @NotEmpty
  private String jwt_key;
  @NotEmpty
  private String cookie_name;
  @NotEmpty
  private String cookie_path;
  @NotEmpty
  private String cookie_domain;
  private int cookie_maxage;
  private boolean cookie_secure;
  private boolean cookie_httponly;
  private String redis_url;
  private int redis_expire = 172800;
  
  @JsonProperty("version")
  public String getVersion() {
    return version;
  }
  public void setVersion(String version) {
    this.version = version;
  }
  @JsonProperty("server")
  public String getServer() {
    return server;
  }
  public void setServer(String server) {
    this.server = server;
  }
  @JsonProperty("jwt_key")
  public String getJwt_key() {
    return jwt_key;
  }
  public void setJwt_key(String jwt_key) {
    this.jwt_key = jwt_key;
  }
  @JsonProperty("cookie_name")
  public String getCookie_name() {
    return cookie_name;
  }
  public void setCookie_name(String cookie_name) {
    this.cookie_name = cookie_name;
  }
  @JsonProperty("cookie_path")
  public String getCookie_path() {
    return cookie_path;
  }
  public void setCookie_path(String cookie_path) {
    this.cookie_path = cookie_path;
  }
  @JsonProperty("cookie_domain")
  public String getCookie_domain() {
    return cookie_domain;
  }
  public void setCookie_domain(String cookie_domain) {
    this.cookie_domain = cookie_domain;
  }
  @JsonProperty("cookie_maxage")
  public int getCookie_maxage() {
    return cookie_maxage;
  }
  public void setCookie_maxage(int cookie_maxage) {
    this.cookie_maxage = cookie_maxage;
  }
  @JsonProperty("cookie_secure")
  public boolean isCookie_secure() {
    return cookie_secure;
  }
  public void setCookie_secure(boolean cookie_secure) {
    this.cookie_secure = cookie_secure;
  }
  @JsonProperty("cookie_httponly")
  public boolean isCookie_httponly() {
    return cookie_httponly;
  }
  public void setCookie_httponly(boolean cookie_httponly) {
    this.cookie_httponly = cookie_httponly;
  }
  @JsonProperty("redis_url")
  public String getRedis_url() {
    return redis_url;
  }
  public void setRedis_url(String redis_url) {
    this.redis_url = redis_url;
  }
  @JsonProperty("redis_expire")
  public int getRedis_expire() {
    return redis_expire;
  }
  public void setRedis_expire(int redis_expire) {
    this.redis_expire = redis_expire;
  }
}
