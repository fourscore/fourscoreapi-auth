/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.filters;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fourscore.auth.config.FourScoreAPIConfiguration;
import io.fourscore.auth.config.FourScoreCookieSecure;
import io.fourscore.auth.utils.FourScoreSecurityContext;
import io.fourscore.auth.utils.FourScoreSecurityUtils;
import io.fourscore.auth.utils.VoterUtils;
import io.fourscore.auth.vo.RESTUser;

@FourScoreCookieSecure
@Provider
@Priority(Priorities.AUTHENTICATION)
public class FourScoreCookieAuthenticationFilter implements ContainerRequestFilter {
  private final Logger log = LoggerFactory.getLogger(FourScoreCookieAuthenticationFilter.class);

  private FourScoreAPIConfiguration fourscoreProperties;
  private Jdbi jdbi;
  
  public FourScoreCookieAuthenticationFilter(FourScoreAPIConfiguration fourscoreProperties,
      Jdbi jdbi) {
    this.fourscoreProperties = fourscoreProperties;
    this.jdbi = jdbi;
  }
  
  @Override
  public void filter(ContainerRequestContext requestContext) throws IOException {
    Cookie fourScoreCookie = null;
    for (Cookie c: requestContext.getCookies().values()) {
      if (c.getName().equals("FourScore")) {
        fourScoreCookie = c;
        break;
      }
    }
    
    if (fourScoreCookie == null) {
      log.error("Missing FourScore Cookie from " + requestContext.getUriInfo()
          .getRequestUri().toString());
      throw new NotAuthorizedException("FourScore cookie must be provided");
    }
    
    String token = fourScoreCookie.getValue();
    if (StringUtils.isBlank(token)) {
      log.error("Missing FourScore Cookie value from " + requestContext.getUriInfo()
          .getRequestUri().toString());
      throw new NotAuthorizedException("FourScore cookie value not set");
    }
    
    final InputStream orig = requestContext.getEntityStream();
    requestContext.setEntityStream(orig);
    
    try {
      String user_name = FourScoreSecurityUtils.getUserNameFromCookie(token,
          fourscoreProperties.getJwt_key());
      String user_type = FourScoreSecurityUtils.getUserTypeFromCookie(token,
          fourscoreProperties.getJwt_key());
      if (StringUtils.isNotBlank(user_name)) {
        VoterUtils vu = new VoterUtils(fourscoreProperties, jdbi);
        if (vu.doesVoterExist(user_name)) {
          String scheme = requestContext.getUriInfo().getRequestUri().getScheme();
          RESTUser user = new RESTUser(user_name);
          List<String> roles = Arrays.asList(user_type);
          user.setRole(roles);
          FourScoreSecurityContext fssc = new FourScoreSecurityContext(user, scheme);
          requestContext.setSecurityContext(fssc);
        } else {
          NewCookie fourOhOneCookie = new NewCookie(fourscoreProperties.getCookie_name(), "",
              fourscoreProperties.getCookie_path(), fourscoreProperties.getCookie_domain(), null,
              0, fourscoreProperties.isCookie_secure(), fourscoreProperties.isCookie_httponly());
          requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
              .cookie(fourOhOneCookie).entity("Could not validate cookie token").build());
        }
      }
    } catch (Exception e) {
      NewCookie fourOhOneCookie = new NewCookie(fourscoreProperties.getCookie_name(), "",
          fourscoreProperties.getCookie_path(), fourscoreProperties.getCookie_domain(), null,
          0, fourscoreProperties.isCookie_secure(), fourscoreProperties.isCookie_httponly());
      requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED)
          .cookie(fourOhOneCookie).entity("Could not validate cookie token").build());
    }
  }
  
  

}
