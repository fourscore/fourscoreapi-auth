/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.fourscore.auth.config.FourScoreAPIConfiguration;

public class FourScoreApiAuthConfiguration extends Configuration {
  @Valid
  @NotNull
  private FourScoreAPIConfiguration fourscoreConfig = new FourScoreAPIConfiguration();

  @Valid
  @NotNull
  private DataSourceFactory database = new DataSourceFactory();
  
  @JsonProperty("fourscore")
  public FourScoreAPIConfiguration getFourscoreConfig() {
    return fourscoreConfig;
  }

  public void setFourscoreConfig(FourScoreAPIConfiguration fourscoreConfig) {
    this.fourscoreConfig = fourscoreConfig;
  }

  @JsonProperty("database")
  public DataSourceFactory getDatabase() {
    return database;
  }

  public void setDatabase(DataSourceFactory database) {
    this.database = database;
  }
}
