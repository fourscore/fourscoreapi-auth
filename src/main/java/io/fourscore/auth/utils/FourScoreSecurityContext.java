/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.utils;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import io.fourscore.auth.vo.RESTUser;

public class FourScoreSecurityContext implements SecurityContext {
  private RESTUser user;
  private String scheme;
  
  public FourScoreSecurityContext(RESTUser user, String scheme) {
    this.user = user;
    this.scheme = scheme;
  }
  
  @Override
  public String getAuthenticationScheme() {
    return SecurityContext.BASIC_AUTH;
  } 

  @Override
  public Principal getUserPrincipal() {
    return this.user;
  }
  
  @Override
  public boolean isSecure() {
    return "https".equals(this.scheme);
  }

  @Override
  public boolean isUserInRole(String arg0) {
    if (user.getRole() != null) {
      return user.getRole().contains(arg0);
    }
    return false;
  }
}
