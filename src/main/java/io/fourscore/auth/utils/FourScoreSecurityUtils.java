/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.utils;

import java.util.Base64;
import java.util.Date;
import java.util.Map;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

public class FourScoreSecurityUtils {
  private static Logger log = LoggerFactory.getLogger(FourScoreSecurityUtils.class);
  
  public static SecretKey decodeSecretKey(String encodedKey){
    SecretKey originalKey = null;
    
    try{
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");
    }catch(Exception e){
        log.error("Exception : ", e);
    }
    
    return originalKey;
  }
  
  /**
   * This function validates a plain text password against the users hash password.
   * 
   * @param plainPassword
   * @param hashedPassword
   * @return
   */
  public static boolean validateBCryptPassword(String plainPassword, String hashedPassword) {
    if (BCrypt.checkpw(plainPassword, hashedPassword)) {
      return true;
    }
    
    return false;
  }
  
  public static String generateJWSToken(String subject, Map<String, Object> jwtClaims,
      int ttlMillis, String jwt_key) {
    String retVal = "";
    
    try {
      Claims claims = Jwts.claims().setSubject(subject);
      claims.putAll(jwtClaims);
      
      SecretKey secretKey = decodeSecretKey(jwt_key);
      long nowMillis = System.currentTimeMillis();
      Date now = new Date(nowMillis);
      JwtBuilder builder = Jwts.builder()
          .setIssuedAt(now)
          .setClaims(claims)
          .setIssuer("https://fourscore.io")
          .signWith(SignatureAlgorithm.HS256, secretKey);
      if (ttlMillis > 0) {
        long expMillis = nowMillis + ttlMillis;
        Date exp = new Date(expMillis);
        builder.setExpiration(exp);
      }
      retVal = builder.compact();
    } catch (Exception e) {
      log.error("Exception : ", e);
    }
    
    return retVal;
  }

  public static String getUserNameFromCookie(String cookieValue, String jwt_key) {
    String user_name = null;
    SecretKey secretKey = decodeSecretKey(jwt_key);
    
    try {
      Jws<Claims> jp = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(cookieValue);
      user_name = jp.getBody().getSubject();
    } catch (SignatureException e) {
      log.error("Signature Exception : ", e);
    } catch (ExpiredJwtException e) {
      log.error("Expired Token. Error : ", e);
    }
    
    return user_name;
  }
  
  public static String getUserTypeFromCookie(String cookieValue, String jwt_key) {
    String user_type = "";
    SecretKey secretKey = decodeSecretKey(jwt_key);
    
    try {
      Jws<Claims> jp = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(cookieValue);
      Map<String, Object> jwtClaims = jp.getBody();
      user_type = (String) jwtClaims.get("user_type");
    } catch (SignatureException e) {
      log.error("Signature Exception : ", e);
    } catch (ExpiredJwtException e) {
      log.error("Expired Token. Error : ", e);
    }
    
    return user_type;
  }
}
