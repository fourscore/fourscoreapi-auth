/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fourscore.auth.config.FourScoreAPIConfiguration;
import io.fourscore.auth.vo.Address_VO;
import io.fourscore.auth.vo.Voter_VO;

public class VoterUtils {
  private static final Logger log = LoggerFactory.getLogger(VoterUtils.class);
  
  private FourScoreAPIConfiguration fourscoreProperties;
  private Jdbi jdbi;
  
  public VoterUtils (FourScoreAPIConfiguration fourscoreProperties, Jdbi jdbi) {
    this.fourscoreProperties = fourscoreProperties;
    this.jdbi = jdbi;
  }
  
  /**
   * This function determines if a voter exists.
   * 
   * @param id          int of the voter id
   * @param userName    String of the voter user name
   * @return            boolean
   */
  public boolean doesVoterExist(String userName) {
    try {
      Voter_VO userInfo = getVoterInfoByUserName(userName);
      if (userInfo != null) {
        return true;
      }
    } catch (Exception e) {
      log.error("Exception : ", e);
    }
    return false;
  }
  
  public Voter_VO getVoterInfoByUserName(String userName) {
    Voter_VO voter = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    try (Handle handle = jdbi.open()) {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT ID, USER_UUID, USER_NAME, PASSWORD, FIRST_NAME, MIDDLE_NAME, ");
      sql.append("LAST_NAME, EMAIL, ADDRESS_1, ADDRESS_2, ADDRESS_3, ADDRESS_4, CITY, COUNTY, ");
      sql.append("STATE, ZIP, SCHOOL_DISTRICT, LATITUDE, LONGITUDE, GOOGLE_PLACE_ID ");
      sql.append("FROM USER WHERE USER_NAME = ?");
      ps = handle.getConnection().prepareStatement(sql.toString());
      ps.setString(1, userName);
      rs = ps.executeQuery();
      
      while (rs.next()) {
        voter = new Voter_VO();
        voter.setId(rs.getLong(1));
        voter.setUser_uuid(rs.getString(2));
        voter.setUser_name(rs.getString(3));
        voter.setPassword(rs.getString(4));
        voter.setFirst_name(rs.getString(5));
        voter.setMiddle_name(rs.getString(6));
        voter.setLast_name(rs.getString(7));
        voter.setEmail(rs.getString(8));
        Address_VO voter_address = new Address_VO();
        voter_address.setAddress1(rs.getString(9));
        voter_address.setAddress2(rs.getString(10));
        voter_address.setAddress3(rs.getString(11));
        voter_address.setAddress4(rs.getString(12));
        voter_address.setCity(rs.getString(13));
        voter_address.setCounty(rs.getString(14));
        voter_address.setState(rs.getString(15));
        voter_address.setZip(rs.getString(16));
        voter_address.setSchool_district(rs.getString(17));
        voter_address.setLatitude(rs.getBigDecimal(18));
        voter_address.setLongitude(rs.getBigDecimal(19));
        voter_address.setPlace_id(rs.getString(20));
        voter.setAddress(voter_address);
      }
    } catch (Exception e) {
      log.error("Exception : ", e);
    }
    
    return voter;
  }
}
