/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fourscore.auth.config.FourScoreAPIConfiguration;

public class CandidateUtils {
  private static final Logger log = LoggerFactory.getLogger(CandidateUtils.class);
  
  private FourScoreAPIConfiguration fourscoreProperties;
  private Jdbi jdbi;
  
  public CandidateUtils(FourScoreAPIConfiguration fourscoreProperties, Jdbi jdbi) {
    this.fourscoreProperties = fourscoreProperties;
    this.jdbi = jdbi;
  }
  
  /**
   * This function determines if a voter is a candidate.
   * 
   * @param userName
   * @return
   */
  public boolean isCandidate(String userName) {
    PreparedStatement ps = null;
    ResultSet rs = null;
    StringBuilder sql = new StringBuilder();
    
    try (Handle handle = jdbi.open()) {
      sql.append("SELECT C.ID FROM CANDIDATE C, USER U WHERE C.USER_ID = U.ID AND ");
      sql.append("U.USER_NAME = ?");
      ps = handle.getConnection().prepareStatement(sql.toString());
      ps.setString(1, userName);
      rs = ps.executeQuery();
      
      while (rs.next()) {
        return true;
      }
    } catch (Exception e) {
      log.error("Exception : ", e);
    }
    
    return false;
  }
}
