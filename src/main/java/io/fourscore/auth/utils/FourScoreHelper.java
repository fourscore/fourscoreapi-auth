/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class FourScoreHelper {
  /**
   * Round a double to the selected places
   * 
   * @param value     double of the value you want to round
   * @param places    int of the number of places
   * @return          double
   */
  public static double round(double value, int places) {
      if (places < 0) throw new IllegalArgumentException();

      BigDecimal bd = new BigDecimal(value);
      bd = bd.setScale(places, RoundingMode.HALF_UP);
      return bd.doubleValue();
  }
}
