/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.utils;

public class FourScoreProperties {
  public static final String USER_TYPE_VOTER = "VOTER";
  public static final String USER_TYPE_CANDIDATE = "CANDIDATE";
  
  public static final String X_API_STATISTICS = "X-API-Statistics";
  public static final String X_API_SERVER = "X-API-Server";
  public static final String X_API_ERROR = "X-API-Error";
  public static final String X_API_ERROR_RESPONSE = "X-API-Error-Response";
}
