/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth.resources;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang3.StringUtils;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.fourscore.auth.config.FourScoreAPIConfiguration;
import io.fourscore.auth.config.FourScoreCookieSecure;
import io.fourscore.auth.utils.CandidateUtils;
import io.fourscore.auth.utils.FourScoreHelper;
import io.fourscore.auth.utils.FourScoreProperties;
import io.fourscore.auth.utils.FourScoreSecurityUtils;
import io.fourscore.auth.utils.VoterUtils;
import io.fourscore.auth.vo.Voter_VO;

@Path("/auth")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class FourScoreAuthApiResource {
  private static final Logger log = LoggerFactory.getLogger(FourScoreAuthApiResource.class);
  
  private FourScoreAPIConfiguration fourscoreProperties;
  private Jdbi jdbi;
  
  public FourScoreAuthApiResource(FourScoreAPIConfiguration fourscoreProperties, Jdbi jdbi) {
    this.fourscoreProperties = fourscoreProperties;
    this.jdbi = jdbi;
  }
  
  @POST
  @Path("/session")
  public Response postSession(
      Voter_VO voterInfo) {
    long start_time = System.nanoTime();
    NewCookie fs_cookie = null;
    boolean valid_login = false;
    Response.ResponseBuilder response = Response.status(Response.Status.OK);
    
    try {
      if (voterInfo == null || StringUtils.isBlank(voterInfo.getUser_name())
          || StringUtils.isBlank(voterInfo.getPassword())) {
        return Response.status(Response.Status.BAD_REQUEST).entity("Missing user name "
            + "or password").build();
      } else {
        VoterUtils vu = new VoterUtils(fourscoreProperties, jdbi);
        Voter_VO voter = vu.getVoterInfoByUserName(voterInfo.getUser_name());
        if (voter != null && StringUtils.isNotBlank(voter.getPassword())) {
          valid_login = FourScoreSecurityUtils.validateBCryptPassword(voterInfo.getPassword(),
              voter.getPassword());
        }
      }
      
      if (valid_login) {
        String user_type = FourScoreProperties.USER_TYPE_VOTER;
        CandidateUtils cu = new CandidateUtils(fourscoreProperties, jdbi);
        if (cu.isCandidate(voterInfo.getUser_name())) {
          user_type = FourScoreProperties.USER_TYPE_CANDIDATE;
        }
        Map<String, Object> jwsClaims = new HashMap<String, Object>();
        jwsClaims.put("user_type", user_type);
        String cookieToken = FourScoreSecurityUtils.generateJWSToken(voterInfo.getUser_name(),
            jwsClaims, 0, fourscoreProperties.getJwt_key());
        fs_cookie = new NewCookie(fourscoreProperties.getCookie_name(), cookieToken,
            fourscoreProperties.getCookie_path(), fourscoreProperties.getCookie_domain(), null,
            fourscoreProperties.getCookie_maxage(), fourscoreProperties.isCookie_secure(),
            fourscoreProperties.isCookie_httponly());
      }
    } catch (Exception e) {
      log.error("Exception : ", e);
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
          .header(FourScoreProperties.X_API_SERVER, fourscoreProperties.getServer()).build();
    }
    
    String strDiff = FourScoreHelper.round((System.nanoTime() - start_time)/1e9, 4) + " secs";
    if (!valid_login) {
      response.status(Response.Status.UNAUTHORIZED);
    } else {
      response.status(Response.Status.CREATED).cookie(fs_cookie);
    }
    
    response.header(FourScoreProperties.X_API_STATISTICS, strDiff);
    response.header(FourScoreProperties.X_API_SERVER, fourscoreProperties.getServer());
    return response.build();
  }

  @PUT
  @Path("/session/switch")
  public Response putSessionSwitch(@Context SecurityContext sc,
      @CookieParam("FourScore") Cookie fourScoreCookie) {
    NewCookie fs_cookie = null;
    long start_time = System.nanoTime();
    String error_description = "";
    Response.ResponseBuilder response = Response.status(Response.Status.OK);
    
    String token = fourScoreCookie.getValue();
    if (StringUtils.isBlank(token)) {
      error_description = "Invalid token.";
      response.status(Response.Status.UNAUTHORIZED);
    } else {
      String user_name = sc.getUserPrincipal().getName().toUpperCase();
      String current_user_type = FourScoreSecurityUtils.getUserTypeFromCookie(token,
          fourscoreProperties.getJwt_key());
      String new_user_type = "";
      if (StringUtils.isBlank(current_user_type)) {
        error_description = "Invalid user type.";
        response.status(Response.Status.UNAUTHORIZED);
      } else {
        if (current_user_type.equalsIgnoreCase(FourScoreProperties.USER_TYPE_VOTER)) {
          CandidateUtils cu = new CandidateUtils(fourscoreProperties, jdbi);
          if (cu.isCandidate(user_name)) {
            new_user_type = FourScoreProperties.USER_TYPE_CANDIDATE;
          } else {
            error_description = "Invalid user.";
            response.status(Response.Status.BAD_REQUEST);
          }
        } else if (current_user_type.equalsIgnoreCase(FourScoreProperties.USER_TYPE_CANDIDATE)) {
          VoterUtils vu = new VoterUtils(fourscoreProperties, jdbi);
          if (vu.doesVoterExist(user_name)) {
            new_user_type = FourScoreProperties.USER_TYPE_VOTER;
          } else {
            error_description = "Invalid user.";
            response.status(Response.Status.BAD_REQUEST);
          }
        }
        
        if (StringUtils.isBlank(new_user_type)) {
          response.status(Response.Status.BAD_REQUEST);
        } else {
          Map<String, Object> jwsClaims = new HashMap<String, Object>();
          jwsClaims.put("user_type", new_user_type);
          
          String cookieToken = FourScoreSecurityUtils.generateJWSToken(user_name, jwsClaims, 0,
              fourscoreProperties.getJwt_key());
          fs_cookie = new NewCookie(fourscoreProperties.getCookie_name(), cookieToken,
              fourscoreProperties.getCookie_path(), fourscoreProperties.getCookie_domain(), null,
              fourscoreProperties.getCookie_maxage(), fourscoreProperties.isCookie_secure(),
              fourscoreProperties.isCookie_httponly());
        }
      }
    }
    
    String strDiff = FourScoreHelper.round((System.nanoTime() - start_time)/1e9, 4) + " secs";
    if (StringUtils.isNotBlank(error_description)) {
      response.header(FourScoreProperties.X_API_ERROR, "ERROR");
      response.header(FourScoreProperties.X_API_ERROR_RESPONSE, error_description);
    } else {
      response.cookie(fs_cookie);
    }
    
    response.header(FourScoreProperties.X_API_STATISTICS, strDiff);
    response.header(FourScoreProperties.X_API_SERVER, fourscoreProperties.getServer());
    return response.build();
  }
  
  @DELETE
  @Path("/session")
  public Response deleteSession(@Context SecurityContext sc) {
    long start_time = System.nanoTime();
    NewCookie fourScoreCookie = new NewCookie(fourscoreProperties.getCookie_name(), "",
        fourscoreProperties.getCookie_path(), fourscoreProperties.getCookie_domain(), null,
        0, fourscoreProperties.isCookie_secure(), fourscoreProperties.isCookie_httponly());
    
    String strDiff = FourScoreHelper.round((System.nanoTime() - start_time)/1e9, 4) + " secs";
    return Response.status(Response.Status.OK).cookie(fourScoreCookie)
        .header(FourScoreProperties.X_API_STATISTICS, strDiff)
        .header(FourScoreProperties.X_API_SERVER, fourscoreProperties.getServer()).build();
  }
  
  @GET
  @FourScoreCookieSecure
  @Path("/session")
  public Response getSession(@Context SecurityContext sc) {
    Voter_VO voter_info = null;
    String error_description = "";
    long start_time = System.nanoTime();
    Response.ResponseBuilder response = Response.status(Response.Status.OK);
    
    try {
      String user_name = sc.getUserPrincipal().getName().toUpperCase();
      VoterUtils vu = new VoterUtils(fourscoreProperties, jdbi);
      voter_info = vu.getVoterInfoByUserName(user_name);
      if (voter_info == null) {
        error_description = "Voter information not found";
      } else {
        
      }
    } catch (Exception e) {
      log.error("Exception : ", e);
    }
    
    String strDiff = FourScoreHelper.round((System.nanoTime() - start_time)/1e9, 4) + " secs";
    if (StringUtils.isNotBlank(error_description)) {
      response.status(Response.Status.BAD_REQUEST).header(FourScoreProperties.X_API_ERROR, "ERROR")
        .header(FourScoreProperties.X_API_ERROR_RESPONSE, error_description);
    } else {
      response.status(Response.Status.OK).entity(voter_info);
    }
    
    response.header(FourScoreProperties.X_API_STATISTICS, strDiff);
    response.header(FourScoreProperties.X_API_SERVER, fourscoreProperties.getServer());
    return response.build();
  }
}
