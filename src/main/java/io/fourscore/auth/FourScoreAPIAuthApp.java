/*******************************************************************************
 * Copyright (c) 2018 Fourscore Tech Inc
 * All rights reserved. 
 *
 * Contributors:
 *     Ben Galaviz <ben@fourscore.io> 
 ******************************************************************************/

package io.fourscore.auth;

import org.jdbi.v3.core.Jdbi;

import io.dropwizard.Application;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Environment;
import io.fourscore.auth.filters.FourScoreCookieAuthenticationFilter;
import io.fourscore.auth.resources.FourScoreAuthApiResource;

public class FourScoreAPIAuthApp extends Application<FourScoreApiAuthConfiguration>{

  public static void main(String[] args) throws Exception {
    new FourScoreAPIAuthApp().run(args);
  }
  @Override
  public void run(FourScoreApiAuthConfiguration configuration, Environment environment)
      throws Exception {
    final JdbiFactory jdbiFactory = new JdbiFactory();
    final Jdbi jdbi = jdbiFactory.build(environment, configuration.getDatabase(), "database");
    
    environment.jersey().register(new FourScoreCookieAuthenticationFilter(
        configuration.getFourscoreConfig(), jdbi));
    environment.jersey().register(new FourScoreAuthApiResource(configuration.getFourscoreConfig(),
        jdbi));
  }
 
}
