########################################################
# Startup script for FourScore Auth API
#
# by : ben.galaviz@lsg-group.com
#
########################################################

export FOURSCOREAPI_JAR=`ls FourScoreAPI-Auth-*.jar`

java -version

java -jar $FOURSCOREAPI_JAR server fourscore.auth.yaml
